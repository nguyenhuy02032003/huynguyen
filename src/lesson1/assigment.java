package lesson1;

import java.util.Date;

public class assigment {
        public static void main(String[] args) {
            System.out.println("khoi tao gia tri 1 account");

            // ............................khoi tao department.........................
            Department department1 = new Department();
            department1.departmentId = 1;
            department1.departmentName = "ban1";

            Department department2 = new Department();
            department2.departmentId = 2;
            department2.departmentName = "ban2";

            Department department3 = new Department();
            department3.departmentId = 3;
            department3.departmentName = "ban3";
            Department[] arrdepartment1 = {department1, department2, department3};
            Department[] arrdepartment2 = {department1, department2};
            Department[] arrdepartment3 = {department1, department3};
            //.................khoi tao position...................................................
            Position position1 = new Position();
            position1.positionId = 1;
            position1.positionName = PositionName.DEV;

            Position position2 = new Position();
            position2.positionId = 2;
            position2.positionName = PositionName.TEST;

            Position position3 = new Position();
            position3.positionId = 3;
            position3.positionName = PositionName.PM;


            //.....................khoi tao account..............................
            Account account1 = new Account();
            account1.accountId = 1;
            account1.department = department1;
            account1.email = "abc1@gmail.com";
            account1.usename = "huy";
            account1.fullName = "nguyen duc huy";
            account1.position = position1;
            account1.createDate = new Date();

            Account account2 = new Account();
            account2.accountId = 2;
            account2.department = department2;
            account2.email = "abc2@gmail.com";
            account2.usename = "binh";
            account2.fullName = "do binh";
            account2.position = position2;
            account2.createDate = new Date();

            Account account3 = new Account();
            account3.accountId = 3;
            account3.department = department3;
            account3.email = "abc3@gmail.com";
            account3.usename = "dat";
            account3.fullName = "dat du";
            account3.position = position3;
            account3.createDate = new Date();
// khoi tao group
            Group group1 = new Group();
            group1.groupId = 1;
            group1.groupName = "nhom 1";
            group1.creator= account1;
            group1.createDate=new Date();
            Group group2= new Group();
            group2.groupId = 2;
            group2.groupName = "nhom 2";
            group2.creator= account2;
            group2.createDate=new Date();
            Group group3= new Group();
            group3.groupId = 3;
            group3.groupName = "nhom 3";
            group3.creator= account3;
            group3.createDate=new Date();
// khởi tạo giá trị cho danh sách group....................
            Group[] arrGr1 = {group2};
// khỏi tạo giá trị cho danh sách account.................
            Account[] arrAccount1 = {account1, account3, account2};
            group1.accounts = arrAccount1;
//................khoi tao groupaccount........................
            GroupAccount groupAccount1=new GroupAccount();
            groupAccount1.account=account1;
            groupAccount1.group=group1;
            groupAccount1.joinDate=new Date();

            GroupAccount groupAccount2=new GroupAccount();
            groupAccount2.account=account2;
            groupAccount2.group=group2;
            groupAccount2.joinDate=new Date();

            GroupAccount groupAccount3=new GroupAccount();
            groupAccount3.account=account3;
            groupAccount3.group=group3;
            groupAccount3.joinDate=new Date();
//....................khoi tao typequestion......................
            TypeQuestion typeQuestion1=new TypeQuestion();
            typeQuestion1.typeId=1;
            typeQuestion1.typeName=TypeName.ESSAY;
            TypeQuestion typeQuestion2=new TypeQuestion();
            typeQuestion2.typeId=2;
            typeQuestion2.typeName=TypeName.MULTIPLECHOICE;
            TypeQuestion typeQuestion3=new TypeQuestion();
            typeQuestion3.typeId=3;
            typeQuestion3.typeName=TypeName.ESSAY;
            //......................khoi tao categoryquestion.....................
            CategoryQuestion categoryQuestion1=new CategoryQuestion();
            categoryQuestion1.categoryId=1;
            categoryQuestion1.categoryName="java";
            CategoryQuestion categoryQuestion2=new CategoryQuestion();
            categoryQuestion2.categoryId=2;
            categoryQuestion2.categoryName="php";
            CategoryQuestion categoryQuestion3=new CategoryQuestion();
            categoryQuestion3.categoryId=3;
            categoryQuestion3.categoryName="ruby";
//........................khoi tao question....................
            Question question1=new Question();
            question1.questionId=1;
            question1.type=typeQuestion1;
            question1.category=categoryQuestion1;
            question1.creator=account1;
            question1.content="gjfeguigeiuhgiuru";
            question1.createDate= new Date();
            Question question2=new Question();
            question2.questionId=2;
            question2.type=typeQuestion2;
            question2.category=categoryQuestion2;
            question2.creator=account2;
            question2.content="gjfeguigeiuhgiggggguru";
            question2.createDate= new Date();
            Question question3=new Question();
            question3.questionId=3;
            question3.type=typeQuestion3;
            question3.category=categoryQuestion3;
            question3.creator=account3;
            question3.content="gjfeguigeiuhgiggrgrwgwegrhrtthhtruru";
            question3.createDate= new Date();
//........................khoi tao answer....................
            Answer answer1= new Answer();
            answer1.answerId=1;
            answer1.question=question1;
            answer1.content="jjjjjjjjjjjjjj";
            answer1.isCorrect=Boolean.TRUE;
            Answer answer2= new Answer();
            answer2.answerId=2;
            answer2.question=question2;
            answer2.content="gggggggggggggggggg";
            answer2.isCorrect=Boolean.FALSE;
            Answer answer3= new Answer();
            answer3.answerId=3;
            answer3.question=question3;
            answer3.content="jjjjjjjjjjjjjj";
            answer3.isCorrect=Boolean.TRUE;
//..................khoi tao exam....................
            Exam exam1=new Exam();
            exam1.examId=1;
            exam1.code="abc";
            exam1.title="giua hoc ki";
            exam1.duration=90;
            exam1.creatorId=1;
            exam1.category=categoryQuestion1;
            exam1.createDate=new Date();
            Exam exam2=new Exam();
            exam2.examId=2;
            exam2.code="bca";
            exam2.title="giua hoc ki";
            exam2.duration=90;
            exam2.creatorId=2;
            exam2.category=categoryQuestion2;
            exam2.createDate=new Date();
            Exam exam3=new Exam();
            exam3.examId=3;
            exam3.code="bac";
            exam3.title="giua hoc ki";
            exam3.duration=90;
            exam3.creatorId=3;
            exam3.category=categoryQuestion3;
            exam3.createDate=new Date();
//.............khoi tao examquestion............................
            ExamQuestion examQuestion1=new ExamQuestion();
            examQuestion1.exam=exam1;
            examQuestion1.question=question1;
            ExamQuestion examQuestion2= new ExamQuestion();
            examQuestion2.exam=exam2;
            examQuestion2.question=question2;
            ExamQuestion examQuestion3= new ExamQuestion();
            examQuestion3.exam=exam3;
            examQuestion3.question=question3;

            //.............................Assigment2..............................................
            System.out.println("....................question 1.............................");
            if (account2.department == null) {
                System.out.println("Nhân viên chưa có phòng ban");
            } else {
                System.out.println("Phòng ban của nhân viên này là :" + account2.department.departmentName);
            }
            System.out.println("............question3.................");
            System.out.println(account2.department == null ? "Nhân viên chưa có phòng ban" : "Phòng ban của nhân viên này là :" + account2.department.departmentName);


            System.out.println(".........question 4............................");
            System.out.println(account1.position.positionName == position1.positionName.DEV ? "Đây là Developer" : "Người này không phải là Developer");


            System.out.println(".............question2..............");
            if (account2.groups.length < 3) {
                System.out.println("Group của nhân viên này là Java Fresher c# Fresher");
            } else if (account2.groups.length == 3) {
                System.out.println("Nhân viên này là người quan trọng tham gia tất cả group");
            } else if (account2.groups.length == 4) {
                System.out.println("Người này là người hòng truyện tham gia tất cả các group");
            } else {
                System.out.println("các trường hợp còn lại");
            }
            System.out.println(".............question5..............");
            int accountLength = group1.accounts.length;
            switch (accountLength) {
                case 1:
                    System.out.println("Nhóm có 1 thành viên");
                    break;
                case 2:
                    System.out.println("Nhóm có 2 thành viên");
                    break;
                case 3:
                    System.out.println("Nhóm có 3 thành viên");
                    break;
                default:
                    System.out.println("Nhóm có nhiều thành viên ");
                    break;
            }
            System.out.println("............question6................");
            switch (account2.groups.length) {
                case 1:
                    System.out.println("Group của nhân viên này là Java Fresher c# Fresher");
                    break;
                case 2:
                    System.out.println("Nhân viên này là người quan trọng tham gia tất cả group");
                    break;
                case 3:
                    System.out.println("Người này là người hòng truyện tham gia tất cả các group");
                    break;
                default:
                    System.out.println("các trường hợp còn lại");
                    break;
            }
            System.out.println("............question8................");
            for (Account account : arrAccount1) {
                System.out.println(account.email);
                System.out.println(account.fullName);
                System.out.println(account.department.departmentName);
            }
            System.out.println("............question9................");
            for (Department department : arrdepartment1) {
                System.out.println(department.departmentId);
                System.out.println(department.departmentName);
            }
            System.out.println("............question10................");
            for (int i = 0; i < arrAccount1.length; i++) {
                System.out.println("...................");
                System.out.println("Thông tin account thứ " + (i + 1) + "là:");
                System.out.println("Email" + arrAccount1[i].email);
                System.out.println("Fullname" + arrAccount1[i].fullName);
                System.out.println("phòng ban:" + arrdepartment1[i].departmentName);
            }
            System.out.println("............question11................");
            for (int i = 0; i < arrdepartment1.length; i++) {
                System.out.println("...................");
                System.out.println("Thông tin phòng ban thứ " + (i + 1) + " là:");
                System.out.println("Id:" + arrdepartment1[i].departmentId);
                System.out.println("Name:" + arrdepartment1[i].departmentName);
            }
            System.out.println("............question12................");
            for (int i = 0; i < arrdepartment2.length; i++) {
                System.out.println("...................");
                System.out.println("Thông tin phòng ban thứ " + (i + 1) + " là:");
                System.out.println("Id:" + arrdepartment2[i].departmentId);
                System.out.println("Name:" + arrdepartment2[i].departmentName);
            }
            System.out.println("............question13................");
            Account[] arraccount2 = {account1, account3};
            for (int i = 0; i < arraccount2.length; i++) {
                System.out.println("...................");
                System.out.println("Thông tin account thứ :" + (i + 1) + "là:");
                System.out.println("Email:" + arraccount2[i].email);
                System.out.println("Fullname:" + arraccount2[i].fullName);
                System.out.println("phòng ban:" + arrdepartment3[i].departmentName);
            }
            System.out.println("............question14................");
            for (int i = 0; i < 3; i++) {
                System.out.println("...................");
                System.out.println("Thông tin account thứ " + (i + 1) + "là:");
                System.out.println("Email" + arrAccount1[i].email);
                System.out.println("Fullname" + arrAccount1[i].fullName);
                System.out.println("phòng ban:" + arrdepartment1[i].departmentName);
            }
            System.out.println("............question16................");
            int number = 0;
            System.out.println("Làm lại q10");
            while (number < arrAccount1.length) {
                System.out.println("...................");
                System.out.println("Thông tin account thứ " + (number + 1) + "là:");
                System.out.println("Email" + arrAccount1[number].email);
                System.out.println("Fullname" + arrAccount1[number].fullName);
                System.out.println("phòng ban:" + arrdepartment1[number].departmentName);
                number++;
            }
            System.out.println("Làm lại q11");
            int num1 = 0;
            while (num1 < arrdepartment1.length) {
                System.out.println("...................");
                System.out.println("Thông tin phòng ban thứ " + (num1 + 1) + " là:");
                System.out.println("Id:" + arrdepartment1[num1].departmentId);
                System.out.println("Name:" + arrdepartment1[num1].departmentName);
                num1++;
            }
            System.out.println("Làm lại q12");
            int num2 = 0;
            while (num2 < 2) {
                System.out.println("...................");
                System.out.println("Thông tin phòng ban thứ " + (num2 + 1) + " là:");
                System.out.println("Id:" + arrdepartment2[num2].departmentId);
                System.out.println("Name:" + arrdepartment2[num2].departmentName);
                num2++;
            }
            System.out.println("Làm lại q13");
            int num3 = 0;
            while (num3 < arrAccount1.length) {
                if (num3 == 2) {continue;}
                System.out.println("...................");
                System.out.println("Thông tin account thứ " + (num3 + 1) + "là:");
                System.out.println("Email" + arrAccount1[num3].email);
                System.out.println("Fullname" + arrAccount1[num3].fullName);
                System.out.println("phòng ban:" + arrdepartment1[num3].departmentName);
                num3++;
            }
            System.out.println("Làm lại q14");
            int num4 = 0;
            while (num4 < 3) {
                System.out.println("...................");
                System.out.println("Thông tin account thứ " + (num4 + 1) + "là:");
                System.out.println("Email" + arrAccount1[num4].email);
                System.out.println("Fullname" + arrAccount1[num4].fullName);
                System.out.println("phòng ban:" + arrdepartment1[num4].departmentName);
                num4++;
            }
            System.out.println("............question17................");
            int num5 = 0;
            System.out.println("Làm lại q10");
            do {System.out.println("...................");
                System.out.println("Thông tin account thứ " + (number + 1) + "là:");
                System.out.println("Email" + arrAccount1[number].email);
                System.out.println("Fullname" + arrAccount1[number].fullName);
                System.out.println("phòng ban:" + arrdepartment1[number].departmentName);
                number++;
            } while (num5 < arrAccount1.length);

            System.out.println("Làm lại q11");
            int num6 = 0;
            do { num6++;
                System.out.println("...................");
                System.out.println("Thông tin phòng ban thứ " + (num6 + 1) + " là:");
                System.out.println("Id:" + arrdepartment1[num6].departmentId);
                System.out.println("Name:" + arrdepartment1[num6].departmentName);
            } while (num6 < arrdepartment1.length);
            System.out.println("Làm lại q12");
            int num7 = 0;
            do {num7++;
                System.out.println("...................");
                System.out.println("Thông tin phòng ban thứ " + (num7 + 1) + " là:");
                System.out.println("Id:" + arrdepartment2[num7].departmentId);
                System.out.println("Name:" + arrdepartment2[num7].departmentName);
            } while (num7 < 2);
            System.out.println("Làm lại q13");
            int num8 = 0;
            do { if (num3 == 2) {continue;}
                System.out.println("...................");
                System.out.println("Thông tin account thứ " + (num8 + 1) + "là:");
                System.out.println("Email" + arrAccount1[num8].email);
                System.out.println("Fullname" + arrAccount1[num8].fullName);
                System.out.println("phòng ban:" + arrdepartment1[num8].departmentName);
                num8++;
            } while (num8 < arrAccount1.length);
            System.out.println("Làm lại q14");
            int num9 = 0;
            do {System.out.println("...................");
                System.out.println("Thông tin account thứ " + (num9 + 1) + "là:");
                System.out.println("Email" + arrAccount1[num9].email);
                System.out.println("Fullname" + arrAccount1[num9].fullName);
                System.out.println("phòng ban:" + arrdepartment1[num9].departmentName);
            } while (num9 < 3);
        }
    }