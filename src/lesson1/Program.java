package lesson1;

import java.util.Date;

public class Program {
    public static void main(String[] args) {
        System.out.println("khoi tao gia tri 1 account");

        // ............................khoi tao department.........................
        Department department1 = new Department();
        department1.departmentId = 1;
        department1.departmentName = "ban1";

        Department department2 = new Department();
        department2.departmentId = 2;
        department2.departmentName = "ban2";

        Department department3 = new Department();
        department3.departmentId = 3;
        department3.departmentName = "ban3";
        System.out.println("In ra Department Name:");
        System.out.println(department1.departmentId + ": " + department1.departmentName);
        System.out.println(department2.departmentId + ": " + department2.departmentName);
        System.out.println(department3.departmentId + ": " + department3.departmentName);

        //.................khoi tao position...................................................
        Position position1 = new Position();
        position1.positionId = 1;
        position1.positionName = PositionName.DEV;

        Position position2 = new Position();
        position2.positionId = 2;
        position2.positionName = PositionName.TEST;

        Position position3 = new Position();
        position3.positionId = 3;
        position3.positionName = PositionName.PM;

        System.out.println("In ra Position Name:");
        System.out.println(position1.positionId + ": " + position1.positionName);
        System.out.println(position2.positionId + ": " + position2.positionName);
        System.out.println(position3.positionId + ": " + position3.positionName);



        //.....................khoi tao account..............................
        Account account1 = new Account();
        account1.accountId = 1;
        account1.department = department1;
        account1.email = "abc1@gmail.com";
        account1.usename = "huy";
        account1.fullName = "nguyen duc huy";
        account1.position = position1;
        account1.createDate = new Date();

        Account account2 = new Account();
        account2.accountId = 2;
        account2.department = department2;
        account2.email = "abc2@gmail.com";
        account2.usename = "binh";
        account2.fullName = "do binh";
        account2.position = position2;
        account2.createDate = new Date();

        Account account3 = new Account();
        account3.accountId = 3;
        account3.department = department3;
        account3.email = "abc3@gmail.com";
        account3.usename = "dat";
        account3.fullName = "dat du";
        account3.position = position3;
        account3.createDate = new Date();
        System.out.println("In ra Department Name của mỗi Account:");
        System.out.println(account1.accountId + ": " + account1.department.departmentName);
        System.out.println(account2.accountId + ": " + account2.department.departmentName);
        System.out.println(account3.accountId + ": " + account3.department.departmentName);


// khoi tao group
        Group group1 = new Group();
        group1.groupId = 1;
        group1.groupName = "nhom 1";
        group1.creator = account1;
        group1.createDate = new Date();
        Group group2 = new Group();
        group2.groupId = 2;
        group2.groupName = "nhom 2";
        group2.creator = account2;
        group2.createDate = new Date();
        Group group3 = new Group();
        group3.groupId = 3;
        group3.groupName = "nhom 3";
        group3.creator = account3;
        group3.createDate = new Date();
        System.out.println(group1.groupId + ": " + group1.groupName);
        System.out.println(group2.groupId + ": " + group2.groupName);
        System.out.println(group3.groupId + ": " + group3.groupName);

//................khoi tao groupaccount........................
        GroupAccount groupAccount1 = new GroupAccount();
        groupAccount1.account = account1;
        groupAccount1.group = group1;
        groupAccount1.joinDate = new Date();

        GroupAccount groupAccount2 = new GroupAccount();
        groupAccount2.account = account2;
        groupAccount2.group = group2;
        groupAccount2.joinDate = new Date();

        GroupAccount groupAccount3 = new GroupAccount();
        groupAccount3.account = account3;
        groupAccount3.group = group3;
        groupAccount3.joinDate = new Date();
        System.out.println("In ra Account Id của Group:");
        System.out.println(groupAccount1.group.groupId + ": " + groupAccount1.account.accountId);
        System.out.println(groupAccount2.group.groupId + ": " + groupAccount2.account.accountId);
        System.out.println(groupAccount3.group.groupId + ": " + groupAccount3.account.accountId);

//....................khoi tao typequestion......................
        TypeQuestion typeQuestion1 = new TypeQuestion();
        typeQuestion1.typeId = 1;
        typeQuestion1.typeName = TypeName.ESSAY;
        TypeQuestion typeQuestion2 = new TypeQuestion();
        typeQuestion2.typeId = 2;
        typeQuestion2.typeName = TypeName.MULTIPLECHOICE;
        TypeQuestion typeQuestion3 = new TypeQuestion();
        typeQuestion3.typeId = 3;
        typeQuestion3.typeName = TypeName.ESSAY;
        System.out.println("In ra Type Name:");
        System.out.println(typeQuestion1.typeId + ": " + typeQuestion1.typeName);
        System.out.println(typeQuestion2.typeId + ": " + typeQuestion2.typeName);
        System.out.println(typeQuestion3.typeId + ": " + typeQuestion3.typeName);

        //......................khoi tao categoryquestion.....................
        CategoryQuestion categoryQuestion1 = new CategoryQuestion();
        categoryQuestion1.categoryId = 1;
        categoryQuestion1.categoryName = "java";
        CategoryQuestion categoryQuestion2 = new CategoryQuestion();
        categoryQuestion2.categoryId = 2;
        categoryQuestion2.categoryName = "php";
        CategoryQuestion categoryQuestion3 = new CategoryQuestion();
        categoryQuestion3.categoryId = 3;
        categoryQuestion3.categoryName = "ruby";
        System.out.println("In ra Category Name:");
        System.out.println(categoryQuestion1.categoryId + ": " + categoryQuestion1.categoryName);
        System.out.println(categoryQuestion2.categoryId + ": " + categoryQuestion2.categoryName);
        System.out.println(categoryQuestion3.categoryId + ": " + categoryQuestion3.categoryName);

//........................khoi tao question....................
        Question question1 = new Question();
        question1.questionId = 1;
        question1.type = typeQuestion1;
        question1.category = categoryQuestion1;
        question1.creator = account1;
        question1.content = "gjfeguigeiuhgiuru";
        question1.createDate = new Date();
        Question question2 = new Question();
        question2.questionId = 2;
        question2.type = typeQuestion2;
        question2.category = categoryQuestion2;
        question2.creator = account2;
        question2.content = "gjfeguigeiuhgiggggguru";
        question2.createDate = new Date();
        Question question3 = new Question();
        question3.questionId = 3;
        question3.type = typeQuestion3;
        question3.category = categoryQuestion3;
        question3.creator = account3;
        question3.content = "gjfeguigeiuhgiggrgrwgwegrhrtthhtruru";
        question3.createDate = new Date();
        System.out.println("In ra TypeName của mỗi Question:");
        System.out.println(question1.questionId + ": " + question1.type.typeName);
        System.out.println(question2.questionId + ": " + question2.type.typeName);
        System.out.println(question3.questionId + ": " + question3.type.typeName);

//........................khoi tao answer....................
        Answer answer1 = new Answer();
        answer1.answerId = 1;
        answer1.question = question1;
        answer1.content = "jjjjjjjjjjjjjj";
        answer1.isCorrect = Boolean.TRUE;
        Answer answer2 = new Answer();
        answer2.answerId = 2;
        answer2.question = question2;
        answer2.content = "gggggggggggggggggg";
        answer2.isCorrect = Boolean.FALSE;
        Answer answer3 = new Answer();
        answer3.answerId = 3;
        answer3.question = question3;
        answer3.content = "jjjjjjjjjjjjjj";
        answer3.isCorrect = Boolean.TRUE;
        System.out.println("In ra nội dung của mỗi Answer:");
        System.out.println(answer1.answerId + ": " + answer1.content);
        System.out.println(answer2.answerId + ": " + answer2.content);
        System.out.println(answer3.answerId + ": " + answer3.content);

//..................khoi tao exam....................
        Exam exam1 = new Exam();
        exam1.examId = 1;
        exam1.code = "abc";
        exam1.title = "giua hoc ki";
        exam1.duration = 90;
        exam1.creatorId = 1;
        exam1.category = categoryQuestion1;
        exam1.createDate = new Date();
        Exam exam2 = new Exam();
        exam2.examId = 2;
        exam2.code = "bca";
        exam2.title = "giua hoc ki";
        exam2.duration = 90;
        exam2.creatorId = 2;
        exam2.category = categoryQuestion2;
        exam2.createDate = new Date();
        Exam exam3 = new Exam();
        exam3.examId = 3;
        exam3.code = "bac";
        exam3.title = "giua hoc ki";
        exam3.duration = 90;
        exam3.creatorId = 3;
        exam3.category = categoryQuestion3;
        exam3.createDate = new Date();
        System.out.println("In ra Duration của mỗi Exam:");
        System.out.println(exam1.examId + ": " + exam1.duration);
        System.out.println(exam2.examId + ": " + exam2.duration);
        System.out.println(exam3.examId + ": " + exam3.duration);

//.............khoi tao examquestion............................
        ExamQuestion examQuestion1 = new ExamQuestion();
        examQuestion1.exam = exam1;
        examQuestion1.question = question1;
        ExamQuestion examQuestion2 = new ExamQuestion();
        examQuestion2.exam = exam2;
        examQuestion2.question = question2;
        ExamQuestion examQuestion3 = new ExamQuestion();
        examQuestion3.exam = exam3;
        examQuestion3.question = question3;
        System.out.println("In ra QuestionId của Exam:");
        System.out.println(examQuestion1.exam.examId + ": " + examQuestion1.question.questionId);
        System.out.println(examQuestion2.exam.examId + ": " + examQuestion2.question.questionId);
        System.out.println(examQuestion3.exam.examId + ": " + examQuestion3.question.questionId);


    }
}